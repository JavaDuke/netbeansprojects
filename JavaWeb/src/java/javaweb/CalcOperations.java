package javaweb;

/**
 *
 * @author javaworkstation
 */
public class CalcOperations {
    
    public static double add(double argument1, double argument2){
        
        return argument1 + argument2;
        
    }
    
    public static double subtract(double argument1, double argument2){
        
        return argument1 - argument2;
        
    }
    
    public static double multiply(double argument1, double argument2){
        
        return argument1 * argument2;
        
    }
    
    public static double divide(double argument1, double argument2){
        
        if (argument1 != 0){
            
            return argument1 / argument2;
            
        }
        return 0;   
    }
    
    //расчет
    public static double calcResult(double argument1, double argument2, OperationType operType){
        
        double result = 0;
        
        switch(operType){
            
            case ADD: {
                
                result = CalcOperations.add(argument1, argument2);
                break;
                
            }
            
            case SUBTRACT: {
                
                result = CalcOperations.subtract(argument1, argument2);
                break;
                
            }
            
            case DIVIDE: {
                
                result = CalcOperations.divide(argument1, argument2);
                break;
                
            }
            
            case MULTIPLY: {
                
                result = CalcOperations.multiply(argument1, argument2);
                break;
                
            }
        }
        
        return result;
        
    }
    
}
