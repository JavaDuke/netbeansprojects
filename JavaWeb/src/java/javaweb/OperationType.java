package javaweb;

/**
 *
 * @author javaworkstation
 */
public enum OperationType {
    
    ADD("+"),
    SUBTRACT("-"),
    MULTIPLY("*"),
    DIVIDE("/");
    
    private String stringValue;
    
    private OperationType(String stringValue){
        
        this.stringValue = stringValue;
        
    }
    
    public String getStringValue(){
        
        return stringValue;
        
    }
    
    
}
