package javaweb;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author javaworkstation
 */
public class JavaWeb extends HttpServlet {

    

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArrayList<String> listOperations;

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Enumeration en = request.getParameterNames();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>JavaWeb</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Servlet - " + request.getContextPath() + "</h1>");
        while (en.hasMoreElements()) {

            String nextElement = en.nextElement().toString();
            out.println("<h1>Parametr: " + nextElement + " = " + request.getParameter(nextElement) + "</h1>");

        }

        try {
            //считывание параметров
            //аргумент1
            double argument1 = Double.valueOf(request.getParameter("argument1").toString()).doubleValue();
            //аргумент2
            double argument2 = Double.valueOf(request.getParameter("argument2").toString()).doubleValue();
            //операция
            String operation = String.valueOf(request.getParameter("operation").toString());

            //определение или создание сессии
            HttpSession session = request.getSession(true);

            //получение типа операции
            OperationType operType = OperationType.valueOf(operation.toUpperCase());

            //расчёт 
            double result = CalcOperations.calcResult(argument1, argument2, operType);

            //для новой сессии создаем новый список из атрибутов сессии
            if (session.isNew()) {

                listOperations = new ArrayList<String>();

            } else {//иначе получаем список из атрибутов уже имеющейся сессии

                listOperations = (ArrayList<String>) session.getAttribute("formula");

            }

            //добавляем новую операцию в список и её атрибут сессии
            listOperations.add(argument1 + " " + operType.getStringValue() + " " + argument2 + " = " + result);
            session.setAttribute("formula", listOperations);

            //вывод всех операций 
            out.println("<h1>ID вашей сессии равен: " + session.getId() + "</h1>");
            out.println("<h3>Список операций (всего: " + listOperations.size() + ") </h3>");

            for (String oper : listOperations) {

                out.println("<h3>" + oper + "</h3>");

            }

        } catch (Exception ex) {

            //предупреждение пользователю в случае ошибки
            out.println("<h3 style=\"color:red;\">Возникла ошибка. Проверьте параметры!</h3>");
            out.println("<h3>Имена параметров  должны быть argument1, argument2, operation!</h3>");
            out.println("<h3>Operation должен принимать одно из четырёх значений: add, subtract, multiply, divide!</h3>");
            out.println("<h3>Значения argument1, argument2 должны быть числами!</h3>");
            out.println("<h1>Пример:</h1>");
            out.println("<h3>?argument1=7&argument2=3&operation=add</h3>");

        }

        out.println("</body>");
        out.println("</html>");
        out.close();

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        processRequest(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        processRequest(request, response);

    }

    @Override
    public String getServletInfo() {

        return "Servlet calculation";

    }

}
